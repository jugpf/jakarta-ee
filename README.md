# History

This history documents the transition of Java EE into a community-controlled technology. It lists only the most important milestones.

* Oracle and Eclipse Foundation agree on Java EE migration
* Eclipse Foundation starts EE4J Top-Level-Project
* Oracle starts donation of Java EE to EE4J Top-Level-Project
* Eclipse Foundation decides to rename Java EE to Jakarta EE
* 2018-09-10 Eclipse Foundation releases JAX-RS 2.1.1
